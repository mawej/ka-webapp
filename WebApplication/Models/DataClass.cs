﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using WebApp.Models;

namespace WebApp.Models
{
    public class DataClass
    {
        public static List<DateValue> SavedData { get; protected set; }
        public static DateValueCollection SortedData;

        public static Boolean AttempToParseFile(string FileName = "data.csv")
        {
            string FilePath = FindDataFile(FileName);

            return ParseCSVFile(FilePath);
        }

        /*
         * Returns path to file path in data folder
         *
         */
        public static string FindDataFile(string FileName)
        {
            //return Path.GetFullPath(HttpContext.Current.Server.MapPath(String.Format("~/App_Data/{0}", FileName)));
            return HttpContext.Current.Server.MapPath(String.Format("~/App_Data/{0}", FileName));
        }

        /*
         * Option to parse CSV file and store data
         * 
         */
        public static Boolean ParseCSVFile(string FileName)
        {
            CultureInfo plPL = new CultureInfo("pl-PL");
            SavedData = null;
            SavedData = new List<DateValue>();

            string line = "";
            string value_helper;
            double value = 0.0;
            DateTime date;

            if (!File.Exists(FileName)) return false;

            StreamReader reader = new StreamReader(new FileStream(FileName, FileMode.Open, FileAccess.Read));
            try
            {
                
                do
                {
                    line = reader.ReadLine();

                    var values = line.Split(',');
                    value_helper = values[2];

#if DEBUG
                    value_helper = value_helper.Replace('.', ','); // workaround for regional info
#endif

                    if (double.TryParse(value_helper, out value) && DateTime.TryParseExact(values[1], "dd-MM-yyyy", plPL, DateTimeStyles.None, out date))
                    {
                        SavedData.Add(new DateValue { Value = value, Date = date });
                    }
                }
                while (reader.Peek() != -1);
            }
            catch
            {
                //File is empty, or another exception happened
            }
            finally
            {
                reader.Dispose();
            }

            SortedData = new DateValueCollection(SavedData, false);
            SortedData.OrderBy(dv => dv.Date);
            return true;
        }
        
        /*
         * Function to handle overall sorting 
         * 
         */
        public static List<string[]> FormatData(List<DateValue> DataToSort)
        {
            List<string[]> OutputList = new List<string[]>();

            string StringAdditional = "";

            foreach (DateValue element in DataToSort)
            {
                StringAdditional = element.Value.ToString();

#if DEBUG
                StringAdditional = StringAdditional.Replace(',', '.'); // workaround for regional info
#endif

                OutputList.Add(new[] { element.Date.ToString(), StringAdditional });

            }

            return OutputList;
        }


        /*
         * Returns range between start and end time, null if didn't find or wrong time stamp
         * 
         */
        public static List<DateValue> GetIndexes(string start, string end)
        {
            CultureInfo plPL = new CultureInfo("pl-PL");

            DateTime DateStart, DateEnd;
            DateValue DateValStart, DateValEnd;

            if (DateTime.TryParseExact(start, "yyyy-MM-dd", plPL, DateTimeStyles.None, out DateStart) 
                && DateTime.TryParseExact(end, "yyyy-MM-dd", plPL, DateTimeStyles.None, out DateEnd))
            {

                DateValStart = SortedData.GetNearest(DateStart);
                DateValEnd = SortedData.GetNearest(DateEnd);

                int index_start = SortedData.FindIndex(a => a.Date == DateValStart.Date);
                int index_end = SortedData.FindIndex(a => a.Date == DateValEnd.Date);

                int amount = index_end - index_start;

                if (index_start >= 0 && index_end >= 0 && amount > 0)
                {
                    return SortedData.GetRange(index_start, index_end - index_start + 1);
                }
            }
            return null;
        }


    }
}