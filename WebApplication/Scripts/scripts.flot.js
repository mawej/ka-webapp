﻿// Max level to have days selected
var max_x_zoom_level = 360000000;
var max_y_zoom_level = 1;
var saved_data = [];
var day_divide = 1;

// Store indexes of exact labels
var attached_data = [];

// Only attempt to make table once
var alreadyDefined = false;

// Main plot options
var optionsTimePlot = {
    xaxis: {
        mode: "time",
        monthNames: ["sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paź", "lis", "gru"],
        dayNames: ["pon", "wto", "śro", "czw", "pią", "sob", "nie"]
    },
    yaxis: {
        tickFormatter: currencyFormatter,
    },
    selection: {
        mode: "x"
    },
    legend: {
        show: true,
        location: "ne"
    }
};

// Create the overview plot
var overviewOptions = {
    legend: {
        show: false
    },
    series: {
        lines: {
            show: true,
            lineWidth: 1
        },
        shadowSize: 0
    },
    xaxis: {
        mode: "time"
    },
    yaxis: {
        tickFormatter: currencyFormatter
    },
    grid: {
        color: "#999"
    },
    selection: {
        mode: "x"
    },


};

function currencyFormatter(v, axis) {
    return v.toFixed(axis.tickDecimals) + " zł";
}

var monthNames =[
  "Styczeń", "Luty", "Marzec",
  "Kwiecień", "Maj", "Czerwiec", "Lipiec",
  "Sierpień", "Wrzesień", "Październik",
  "Listopad", "Grudzień"
  ];

function formatDate(date) {
    var day = date.getDate();
    var monthIndex = date.getMonth()+1;
    var year = date.getFullYear();

    if (day <= 9)
        day = "0" + day;

    if (monthIndex <= 9)
        monthIndex = "0" +monthIndex;

    //return day + ' ' +monthNames[monthIndex]+ ' ' + year;
    return year + "-" + monthIndex + "-" + day;
}


function onDataReceived(series) {
    performUpdate(series);
    $("#loadingPage").hide();
}

function showLoading() {
    $("#loadingPage").show();
}

function performUpdate(series) {
    if (typeof (series) == "object")
    {
        if(typeof(series.error) == "string")
        {
            $("#warning_container").text(series.error);
            $("#required_notification").text(series.error);
            
            $("#warnings").show();
            $("#fade").show();
            return;
        }
    }
    $("#required_notification").text("");

    var in_data = [];
    if (typeof (series.firstEntry) != "undefined") {
        saved_data = [];

        if (typeof (series.secondEntry) != "undefined") {
            performUpdate(series.secondEntry);
        }
        performUpdate(series.firstEntry);
        return;
    }

    if (typeof (series.data) == "string") {
        var unserialized = JSON.parse(series.data);
        
        var num_helper = "";
        var first_entry = new Date(unserialized[0][0]);
        var last_entry = new Date(unserialized[unserialized.length - 1][0]);

        for (var i = 0; i < unserialized.length; i++) {
            num_helper = unserialized[i][1];
            //num_helper = num_helper.replace(",", "."); // another regional workaround

            in_data.push([new Date(unserialized[i][0]).getTime(), parseFloat(num_helper)]);
        }

        if (first_entry > last_entry) {
            $("#start_time").val(formatDate(last_entry));
            $("#end_time").val(formatDate(first_entry));
        } else {
            $("#start_time").val(formatDate(first_entry));
            $("#end_time").val(formatDate(last_entry));
        }

        series.data = in_data;
    }

    for (var element = 0; element < saved_data.length; element++) {
        if (saved_data[element].label == series.label) {
            saved_data.splice(element, 1);
            break;
        }
    }

    saved_data.push(series);


    plot = $.plot("#placeholder", saved_data, optionsTimePlot);
    overview = $.plot("#overview", saved_data, overviewOptions);

 
    var table_data = [];
    var element_count = 0;

    // Way to decrease amount of elements in table
    if (day_divide <= 1) day_divide = 1;
    if (day_divide > 365) day_divide = 365;
    day_divide = Math.floor(day_divide);

    for (var element = 0; element < saved_data.length; element++) {
        for (var i = 0; i < saved_data[element].data.length; i++) {
            if (i % day_divide == 0) {
                table_data[element_count++] = {
                    [0]: formatDate(new Date(saved_data[element].data[i][0])),
                    [1]: saved_data[element].data[i][1]
                };
            }
        }
    }

    if (!alreadyDefined) {
        alreadyDefined = true;
        $('#dataTable').DataTable({
            // polish names in table
            data: table_data,
            columns: [
                { title: "Data" },
                { title: "Wartość akcji [zł]" },
            ], language: {
                processing: "Przetwarzanie...",
                search: "Szukaj:",
                lengthMenu: "Pokaż _MENU_ pozycji",
                info: "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                infoEmpty: "Pozycji 0 z 0 dostępnych",
                infoFiltered: "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                infoPostFix: "",
                loadingRecords: "Wczytywanie...",
                zeroRecords: "Nie znaleziono pasujących pozycji",
                emptyTable: "Brak danych",
                paginate: {
                    first: "Pierwsza",
                    previous: "Poprzednia",
                    next: "Następna",
                    last: "Ostatnia"
                },
                aria: {
                    sortAscending: ": aktywuj, by posortować kolumnę rosnąco",
                    sortDescending: ": aktywuj, by posortować kolumnę malejąco"
                }


            }
        });
    }
    
}

$(function () {

    // now connect the two
    $("#placeholder").bind("plotselected", function (event, ranges) {

        // clamp the zooming to prevent eternal zoom

        if (ranges.xaxis.to - ranges.xaxis.from < max_x_zoom_level) {
            ranges.xaxis.to = ranges.xaxis.from + max_x_zoom_level;
        }

        if (ranges.yaxis.to - ranges.yaxis.from < max_y_zoom_level) {
            ranges.yaxis.to = ranges.yaxis.from + max_y_zoom_level;
        }

        // do the zooming

        //plot = $.plot("#placeholder", getData(ranges.xaxis.from, ranges.xaxis.to),
        plot = $.plot("#placeholder", saved_data,
            $.extend(true, {}, optionsTimePlot, {
                xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
                yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
            })
        );

        // don't fire event on the overview to prevent eternal loop

        overview.setSelection(ranges, true);
    });


    $("#overview").bind("plotselected", function (event, ranges) {
        plot.setSelection(ranges);
    });

    $("#fade").click(function () {
        $("#fade").hide();
        $("#warnings").hide();
    });

    $("#warnings").click(function () {
        $("#fade").hide();
        $("#warnings").hide();
    });
});