﻿$(function () {

    $("button.getSelectedData:first").click(function () {

        var start = $("input#start_time").val();
        var end = $("input#end_time").val();
        var percentage = $("input#percentage").val();
        var investment = $("input#investment").val();
        var capitalization = $("input#capitalization").val();

        $.ajax({
            url: "/api/calculate",
            type: "GET",
            dataType: "json",
            data: { start: start, end: end, percentage: percentage, investment: investment, capitalization: capitalization },
            success: onDataReceived,
            beforeSend: showLoading
        });
    });

    $("button.getSelectedData:first").click();

});