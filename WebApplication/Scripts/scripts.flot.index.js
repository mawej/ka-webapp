﻿$(function ()
{

    $("button.fetchSeries").click(function () {

        var button = $(this);

        // Find the URL in the link right next to us, then fetch the data

        $.ajax({
            url: "/api/all",
            type: "GET",
            dataType: "json",
            success: onDataReceived,
            beforeSend: showLoading
        });
    });


    $("button.getSelectedData:first").click(function () {

        var button = $(this);

        // Find the URL in the link right next to us, then fetch the data


        var start = $("input#start_time").val();
        var end = $("input#end_time").val();

        $.ajax({
            url: "/api/select",
            type: "GET",
            dataType: "json",
            data: { start: start, end: end },
            success: onDataReceived,
            beforeSend: showLoading
        });
    });

    $("button.fetchSeries:first").click();

    
    

});