﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class GetController : Controller
    {

        /*
         * One interface for generating unique warning messages for javascript to interpret
         * 
         */
        [ChildActionOnly]
        public JsonResult ReturnJSONMessage(string message)
        {
            return Json(new { error = message }, JsonRequestBehavior.AllowGet);
        }

        /*
         * Get all data
         * 
         */
        [OutputCache(Duration = 60)]
        public JsonResult All()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();

            // it should make SavedData not null if there is file
            if (DataClass.SavedData == null || DataClass.SavedData.Count() == 0)
            {
                DataClass.AttempToParseFile();
            }

            // no data.csv file
            if (DataClass.SavedData.Count() == 0)
            {
                return ReturnJSONMessage("Brak danych");
            }


            List<string[]> OutputList = DataClass.FormatData(DataClass.SavedData);

            return Json(new
            {
                label = "Kurs giełdowego funduszu inwestycyjnego",
                data = js.Serialize(OutputList)
            }, JsonRequestBehavior.AllowGet);
        }


        /*
         * Extract data from CSV file between start and end time
         *
         */
        public JsonResult Select(string start, string end)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            if (DataClass.SortedData != null)
            {
                List<DateValue> Range = DataClass.GetIndexes(start, end);

                if (Range != null)
                {
                    List<string[]> output = DataClass.FormatData(Range);

                    return Json(new
                    {
                        label = "Kurs giełdowego funduszu inwestycyjnego",
                        data = js.Serialize(output)
                    }, JsonRequestBehavior.AllowGet);

                }
                return ReturnJSONMessage("Nieprawdiłowy zakres czasu");
            }

            return ReturnJSONMessage("Nieodpowiedni format danych wejściowych rrrr-mm-dd");
        }

        /*
         * Get results for bank investment
         * 
         */
        public JsonResult Calculate(string start, string end, string percentage, string investment, string capitalization)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            if (DataClass.SortedData != null)
            {
                double PercentageVal, InvestmentVal, CapitalizationVal;

                if (start == null || end == null || percentage == null || investment == null || capitalization == null)
                {
                    return ReturnJSONMessage("Brak danych wejściowych");
                }

#if DEBUG
                percentage = percentage.Replace('.', ','); // workaround for regional info
#endif

#if DEBUG
                investment = investment.Replace('.', ','); // workaround for regional info
#endif

#if DEBUG
                capitalization = capitalization.Replace('.', ','); // workaround for regional info
#endif

                if (double.TryParse(percentage, out PercentageVal) && double.TryParse(investment, out InvestmentVal) && double.TryParse(capitalization, out CapitalizationVal))
                {
                    // Investment value
                    if (InvestmentVal <= 0.0) InvestmentVal = 0.0;
                    if (InvestmentVal > 99999999.99) InvestmentVal = 99999999.99;

                    // Capitalization value
                    if (CapitalizationVal <= 0.0 || CapitalizationVal >= 365.0)
                        return ReturnJSONMessage("Nieprawdiłowy kapitalizacja odsetek");

                    // PercentageVal value
                    if (PercentageVal < 0.0 || PercentageVal > 1.0)
                        return ReturnJSONMessage("Nieprawdiłowy procent lokaty");

                    List<DateValue> Range = DataClass.GetIndexes(start, end);

                    if (Range != null)
                    {
                        // One element for editing purposes
                        DateValue helperElement;

                        // Copy lists for data changing easier
                        List<DateValue> StockInvestment = new List<DateValue>(Range);
                        List<DateValue> BankInvestment = new List<DateValue>(Range);


                        /*
                         * Idea is based, you buy stocks for start period price for how much you money have
                         * 
                         */
                        double StockStartPrice = StockInvestment[0].Value;
                        double StockAmountBought = Math.Floor(InvestmentVal / StockStartPrice);
                        double StockCurrentPrice;

                        for (int element = 0; element < StockInvestment.Count; element++)
                        {
                            helperElement = StockInvestment[element];

                            StockCurrentPrice = StockAmountBought * StockInvestment[element].Value;

                            helperElement.Value = StockCurrentPrice;

                            StockInvestment[element] = helperElement;
                        }

                        // Put data together
                        List<string[]> FormatedInvestment = DataClass.FormatData(StockInvestment);

                        var firstEntry = new
                        {
                            label = String.Format("Zysk z funduszu inwestycyjnego o wkładzie {0} zł, koszt zakupu {1} zł, kupiono {2}", InvestmentVal, StockStartPrice, StockAmountBought),
                            data = js.Serialize(FormatedInvestment)
                        };


                        /*
                         * Calculate profits based on bank investment
                         * 
                         */
                        DateTime LastCapitalizationDay = BankInvestment[0].Date;

                        double CapitalizationDayAmount = 365.0;
                        double CapitalizationDayPeriod = CapitalizationVal;

                        double CapitalizationRatio = CapitalizationDayPeriod / CapitalizationDayAmount;


                        int difference;
                        double BankStartValue = InvestmentVal;
                        double BankLastValue = BankStartValue;
                        double BankPercentage = 0.0;

                        for (int element = 0; element < BankInvestment.Count; element++)
                        {
                            helperElement = BankInvestment[element];

                            // Calculate difference in days between last capitalization days
                            difference = (helperElement.Date - LastCapitalizationDay).Days;

                            if (difference > CapitalizationDayPeriod)
                            {
                                LastCapitalizationDay = helperElement.Date;

                                BankPercentage = BankLastValue * PercentageVal * CapitalizationRatio;

                                BankLastValue = BankPercentage + BankLastValue;
                            }

                            helperElement.Value = BankLastValue;

                            BankInvestment[element] = helperElement;
                        }

                        List<string[]> FormatedBank = DataClass.FormatData(BankInvestment);
                        var secondEntry = new
                        {
                            label = String.Format("Zysk z {0}% (kapitalizacja co {2} dni) lokaty bankowej o wkładzie początkowym {1} zł", PercentageVal * 100.0, InvestmentVal, CapitalizationDayPeriod),
                            data = js.Serialize(FormatedBank)
                        };

                        return Json(
                        new
                        {
                            firstEntry,
                            secondEntry
                        }, JsonRequestBehavior.AllowGet);
                    }
                    return ReturnJSONMessage("Nieprawdiłowy zakres czasu");


                }
                return ReturnJSONMessage("Nieprawidłowe liczby");
            }

            return ReturnJSONMessage("Nieodpowiedni format danych wejściowych rrrr-mm-dd");
        }


    }
}
